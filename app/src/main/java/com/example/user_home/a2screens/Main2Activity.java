package com.example.user_home.a2screens;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.TextView;

import static com.example.user_home.a2screens.R.string.hint_login;

public class Main2Activity extends AppCompatActivity {


    TextView hello;
    TextView checker;
    RelativeLayout color;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        checker = (TextView) findViewById(R.id.checker);
        hello = (TextView) findViewById(R.id.hello1);
        color = (RelativeLayout) findViewById(R.id.color);
        String sec = "";

        Intent intent = getIntent();
        String username = intent.getExtras().getString("login");
        int validator = intent.getExtras().getInt("validator");
        if(validator == 2){
            //color.setBackgroundColor(Color.parseColor("33ff00"));
            sec = "Уровень сложности пароля: простой";
            color.setBackgroundColor(getResources().getColor(R.color.colorOrange));
        }
        if(validator == 3){
            sec = "Уровень сложности пароля: средний";
            color.setBackgroundColor(getResources().getColor(R.color.colorYellow));
        }
        if(validator == 4){
            sec = "Уровень сложности пароля: сложный";
            color.setBackgroundColor(getResources().getColor(R.color.colorGreen));
        }

        hello.setText("Привет, " + username);
        checker.setText(sec);


    }
}
