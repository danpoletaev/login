package com.example.user_home.a2screens;



import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.GetChars;
import android.view.Gravity;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.R.attr.duration;
import static android.R.attr.fingerprintAuthDrawable;
import static android.R.attr.start;
import static android.R.attr.textEditNoPasteWindowLayout;
import static android.R.attr.visibility;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    Button button;
    EditText login;
    EditText password;
    EditText repassword;
    SharedPreferences preferences;
    String username;
    String save;
    Button find;
    CheckBox checkbox;

    public static final String KEY_PREF = "UserData";
    //public static final String KEY_PREF_Check = "password";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        login = (EditText) findViewById(R.id.login);
        password = (EditText) findViewById(R.id.pas);
        repassword = (EditText) findViewById(R.id.rep);


        button = (Button) findViewById(R.id.button1);
        button.setOnClickListener(this);

        checkbox = (CheckBox) findViewById(R.id.checkBox2);

        find = (Button) findViewById(R.id.find);
        find.setOnClickListener(this);

        preferences = getSharedPreferences(KEY_PREF, MODE_PRIVATE);




    }


    public boolean isValid() {
        boolean valid = false;

        if (password.length() >= 6) {
            valid = true;
        }
        return valid;
    }

    public boolean isEquals() {
        boolean equals = false;

        if (password.equals(repassword)) {
            equals = true;
        }
        return equals;
    }


    public String showToast() {
        Toast toast = Toast.makeText(getApplicationContext(),
                "Проверьте поля!",
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        return toast.toString();
    }

    public void onClick(View view) {
        username = login.getText().toString();
        save = password.getText().toString();


        switch (view.getId()) {
            case R.id.button1:
                SharedPreferences.Editor editor = preferences.edit();
                //editor.putString("Username", username);
                if(checkbox.isChecked()) {
                    editor.putString(username, save);
                    editor.commit();
                    editor.apply();
                }


                int length = login.length();
                String pas1 = password.getText().toString();
                String repas = repassword.getText().toString();
                char[] par = pas1.toCharArray();
                int numCount = 0;
                int capCount = 0;
                int smallCount = 0;
                int symCount = 0;
                int validator = 0;
                for (int i = 0; i < password.length(); i++) {
                    if ((par[i] > 47 && par[i] < 58)) {
                        numCount++;
                    }
                    if ((par[i] > 64 && par[i] < 91)) {
                        capCount++;
                    }
                    if ((par[i] > 96 && par[i] < 123)) {
                        smallCount++;
                    }
                    if ((par[i] > 32 && par[i] < 48) || par[i] == 64) {
                        symCount++;
                    }
                }

                if (numCount > 0) {
                    numCount = 1;
                }
                if (capCount > 0) {
                    capCount = 1;
                }
                if (smallCount > 0) {
                    smallCount = 1;
                }
                if (symCount > 0) {
                    symCount = 1;
                }
                validator = numCount + capCount + smallCount + symCount;


                if (length > 0 && isValid() == true && pas1.equals(repas) == true && validator > 1) {

                    String username = login.getText().toString();
                    Intent intent = new Intent(this, Main2Activity.class);
                    intent.putExtra("login", username);
                    intent.putExtra("validator", validator);

                    startActivity(intent);

                } else {

                    showToast();

                }
                break;
            case R.id.find:
                //if (!username.equals("")) {  mCounter = mSettings.getInt(APP_PREFERENCES_COUNTER, 0);
                if (!username.equals("")) {
                    save = preferences.getString(username, "");
                    //SharedPreferences preferences = getSharedPreferences("UserData", 0);
                    //String save = preferences.getString("Password", "");
                }
                password.setText(String.valueOf(save));
                repassword.setText(String.valueOf(save));
                break;



        }
    }
}

